<?php


function parse($filePath)
{
    $fileString = fopen($filePath, 'r');
    $fileStrings = [];

    while (!feof($fileString)) {
        $line = fgets($fileString);
        $fileStrings[] = $line;
    }

    fclose($fileString);

    $explodedStrings = [];
    foreach ($fileStrings as $string) {
        if ($string)
            $explodedStrings[] = explode(' ', $string);
    }

    $returnData['max'] = $explodedStrings[0][0];
    $returnData['numberOfTypes'] = $explodedStrings[0][1];
    unset($explodedStrings[0]);

    $returnData['pizzas'] = $explodedStrings[1];

    return $returnData;
}


