<?php

require_once __DIR__ . '/parse.php';
require_once __DIR__ . '/findNearest.php';
// File: 'a_example', 'b_small', 'c_medium', 'd_quite_big', 'e_also_big'
$fileName = 'c_medium';

$parsedData = parse(__DIR__ . '/inputs/' . $fileName . '.in');
$answer = [];
$indexes = [];

while ($parsedData['max'] > 0) {
    $nearestSliceKey = findNearest($parsedData['pizzas'], (int)$parsedData['max']);

    if ($nearestSliceKey == -1) {
        $nearestSliceKey = findNearest($parsedData['pizzas'], (int)$parsedData['max'], 0);
        if ($nearestSliceKey == -1) break;
    }

    if ($parsedData['max'] - (int)$parsedData['pizzas'][$nearestSliceKey] < 0) {
        break;
    }

    $answer[] = (int)$parsedData['pizzas'][$nearestSliceKey];
    $indexes[] = $nearestSliceKey;
    $parsedData['max'] -= (int)$parsedData['pizzas'][$nearestSliceKey];
    $parsedData['numberOfTypes']--;

    $parsedData['pizzas'][$nearestSliceKey] = '99999999999999999999999';

    if ($parsedData['numberOfTypes'] == 0 || $parsedData['max'] <= 0) {
        break;
    }
}

$document = '';
$document .= count($indexes) . "\n";

foreach ($indexes as $orderItem) {
    $document .= $orderItem . ' ';
}

file_put_contents(__DIR__ . '/results/' . $fileName . '.txt', $document);
print_r(array_sum($answer));
