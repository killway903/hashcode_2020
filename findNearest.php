<?php


function findNearest($array, $value, $method = 1) {
    $count = count($array);

    if($count == 0) {
        return null;
    }

    $div_step               = 2;
    $index                  = ceil($count / $div_step);
    $best_index             = -1;
    $best_score             = null;
    $direction              = null;
    $indexes_checked        = Array();

    while(true) {
        if(isset($indexes_checked[$index])) {
            break ;
        }

        $curr_key = $array[$index] ?? null;
        if($curr_key === null) {
            break ;
        }

        $indexes_checked[$index] = true;

        // perfect match, nothing else to do
        if($curr_key == $value) {
            return $curr_key;
        }

        $prev_key = $array[$index - 1] ?? null;
        $next_key = $array[$index + 1] ?? null;

        switch($method) {
            default:
            case 0:
                $curr_score = abs($curr_key - $value);

                $prev_score = $prev_key !== null ? abs($prev_key - $value) : null;
                $next_score = $next_key !== null ? abs($next_key - $value) : null;

                if($prev_score === null) {
                    $direction = 1;
                }else if ($next_score === null) {
                    break 2;
                }else{
                    $direction = $next_score < $prev_score ? 1 : -1;
                }
                break;
            case 1:
                $curr_score =(int) $curr_key - (int)$value;
                if($curr_score > 0) {
                    $curr_score = null;
                }else{
                    $curr_score = abs($curr_score);
                }

                if($curr_score === null) {
                    $direction = -1;
                }else{
                    $direction = 1;
                }
                break;
        }

        if(($curr_score !== null) && ($curr_score < $best_score) || ($best_score === null)) {
            $best_index = $index;
            $best_score = $curr_score;
        }

        $div_step *= 2;
        $index += $direction * ceil($count / $div_step);
    }

    return $best_index;
}


